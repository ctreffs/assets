#target AfterEffects

/**************************************
Scene : Scene
Resolution : 1920 x 1080
Duration : 10.416667
FPS : 24.000000
Date : 2019-01-22 21:01:24.558500
Exported with io_export_after_effects.py
**************************************/



function compFromBlender(){

var compName = prompt("Blender Comp's Name \nEnter Name of newly created Composition","BlendComp","Composition's Name");
if (compName){
var newComp = app.project.items.addComp(compName, 1920, 1080, 1.000000, 10.416667, 24.000000);
newComp.displayStartTime = 0.083333;


// **************  CAMERA 3D MARKERS  **************


// **************  OBJECTS  **************


var _Cube = newComp.layers.addNull();
_Cube.threeDLayer = true;
_Cube.source.name = "_Cube";
_Cube.property("position").setValue([1067.142854,-157.272778,317.501688],);
_Cube.property("orientation").setValue([-90.000000,-0.000000,99.572724],);
_Cube.property("scale").setValue([99.999994,100.000000,99.999994],);


// **************  LIGHTS  **************


// **************  CAMERAS  **************



}else{alert ("Exit Import Blender animation data \nNo Comp's name has been chosen","EXIT")};}


app.beginUndoGroup("Import Blender animation data");
compFromBlender();
app.endUndoGroup();


