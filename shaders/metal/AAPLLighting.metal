/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Lighting tile shaders
*/

#import "AAPLLightingCommon.h"
#import "AAPLShaderCommon.h"

//------------------------------------------------------------------------------

// Toggled to enable debug rendering to reduce the cost of the default shading
//  by not caching values for possible debug output.
constant bool gEnableDebugView  [[function_constant(ENABLE_DEBUG_VIEW_FC_INDEX)]];

constant bool gUseLightCluster [[function_constant(USE_LIGHT_CLUSTER_FC_INDEX)]];

constant bool gUseRasterizationRate [[function_constant(RASTERIZATION_RATE_FC_INDEX)]];

//------------------------------------------------------------------------------

// Performs tiled lighting, reading the GBuffer from either tile memory or
//  output textures.
// Executes after FSQuadVertexShader so that it renders as a full screen pass
//  with an input texcoord in screen space.
fragment xhalf4 tiledLightingShader(AAPLSimpleTexVertexOut in                       [[ stage_in ]],
                                    constant AAPLUniforms & uniforms                [[ buffer(AAPLBufferIndexUniforms) ]],
                                    constant AAPLCameraUniforms & cameraUniforms    [[ buffer(AAPLBufferIndexCameraUniforms) ]],
#if SUPPORT_RASTERIZATION_RATE
                                    constant rasterization_rate_map_data * rrData   [[ buffer(AAPLBufferIndexRasterizationRateUniforms), function_constant(gUseRasterizationRate) ]],
#endif
                                    constant AAPLPointLightData * pointLightBuffer  [[ buffer(AAPLBufferIndexPointLights) ]],
                                    constant AAPLSpotLightData * spotLightBuffer    [[ buffer(AAPLBufferIndexSpotLights) ]],

                                    constant uint8_t * pointLightIndices  [[ buffer(AAPLBufferIndexPointLightIndices) ]],
                                    constant uint8_t * spotLightIndices   [[ buffer(AAPLBufferIndexSpotLightIndices) ]],

#if USE_FBFETCH_GBUFFER
                                    AAPLGBufferFragOut fb,
#else
                                    texture2d<xhalf, access::sample> gBufferTex0     [[texture(0)]],
                                    texture2d<xhalf, access::sample> gBufferTex1     [[texture(1)]],
                                    texture2d<xhalf, access::sample> gBufferTex2     [[texture(2)]],
                                    texture2d<xhalf, access::sample> gBufferTex3     [[texture(3)]],
#endif
                                    depth2d<float, access::sample>   inDepth         [[texture(4)]],
                                    depth2d_array<float, access::sample> shadowMap   [[texture (5)]],
#if USE_SCATTERING_VOLUME
                                    texture3d<xhalf, access::sample> scattering         [[texture(6)]],
#endif
                                    texture2d<xhalf, access::sample> dfgLutTex          [[texture(7)]],
                                    texturecube<xhalf, access::sample> envMap           [[texture(8)]],
#if USE_SAO
                                    texture2d<xhalf, access::sample> saoTexture         [[texture(9)]],
#endif
#if USE_SPOT_LIGHT_SHADOWS
                                    depth2d_array<float, access::sample> spotShadowMaps [[texture(10)]],
#endif
                                    texture2d<float, access::read> blueNoiseTexture     [[texture(11)]]
                                    )
{
    constexpr sampler nearestSampler(mip_filter::nearest, mag_filter::nearest, min_filter::nearest, address::clamp_to_edge);
    constexpr sampler linearSampler(mip_filter::linear, mag_filter::linear, min_filter::linear, address::clamp_to_edge);

    float2 physicalCoord = in.texCoord;
#if SUPPORT_RASTERIZATION_RATE
    if (gUseRasterizationRate)
    {
        // We are currently drawing inside compressed space, so we have to fix up screen space.
        rasterization_rate_map_decoder decoder(*rrData);
        physicalCoord = decoder.map_screen_to_physical_coordinates(physicalCoord * uniforms.screenSize) * uniforms.invPhysicalSize;
    }
#endif

#if USE_SAO
    float aoSample = saoTexture.sample(nearestSampler, physicalCoord).x;
#else
    float aoSample = 1.0f;
#endif

#if USE_FBFETCH_GBUFFER
    xhalf4 albedoSample = fb.gBuffer0;
    xhalf4 normalSample = fb.gBuffer1;
    xhalf4 emissiveSample = fb.gBuffer2;
    xhalf4 F0RoughnessSample = fb.gBuffer3;
#else
    xhalf4 albedoSample = gBufferTex0.sample(nearestSampler, physicalCoord);
    xhalf4 normalSample = gBufferTex1.sample(nearestSampler, physicalCoord);
    xhalf4 emissiveSample = gBufferTex2.sample(nearestSampler, physicalCoord);
    xhalf4 F0RoughnessSample = gBufferTex3.sample(nearestSampler, physicalCoord);
#endif

    AAPLPixelSurfaceData surfaceData;
    surfaceData.normal      = (xhalf3)normalize((float3)normalSample.xyz); // normalizing half3 normal causes banding
    surfaceData.albedo      = albedoSample.xyz;
    surfaceData.F0          = mix(F0RoughnessSample.xyz, (xhalf)0.02, (xhalf)uniforms.wetness);
    surfaceData.roughness   = mix(F0RoughnessSample.w, (xhalf)0.1, (xhalf)uniforms.wetness);
    surfaceData.alpha       = 1.0f;
    surfaceData.emissive    = emissiveSample.rgb;

    const float depth = inDepth.sample(nearestSampler, physicalCoord);
    float4 worldPosition = worldPositionForTexcoord(in.texCoord, depth, cameraUniforms);

    uint tileIdx;
    if (gUseLightCluster)
    {
        uint tileX = in.position.x / LIGHT_CLUSTERING_TILE_SIZE;
        uint tileY = in.position.y / LIGHT_CLUSTERING_TILE_SIZE;

#if LOCAL_LIGHT_SCATTERING
        uint cluster = zToScatterDepth(linearizeDepth(cameraUniforms, depth)) * LIGHT_CLUSTER_DEPTH;
        cluster = min(cluster, LIGHT_CLUSTER_DEPTH-1u);
#else
        float depthStep = LIGHT_CLUSTER_RANGE / LIGHT_CLUSTER_DEPTH;
        uint cluster = linearizeDepth(cameraUniforms, depth) / depthStep;
        cluster = min(cluster, LIGHT_CLUSTER_DEPTH-1u);
#endif

        tileIdx = (tileX + uniforms.lightIndicesParams.y * tileY + uniforms.lightIndicesParams.z * cluster) * MAX_LIGHTS_PER_CLUSTER;
    }
    else
    {
        uint tileX = in.position.x / LIGHT_CULLING_TILE_SIZE;
        uint tileY = in.position.y / LIGHT_CULLING_TILE_SIZE;
        tileIdx = (tileX + uniforms.lightIndicesParams.x * tileY) * MAX_LIGHTS_PER_TILE;
    }

    pointLightIndices += tileIdx;
    spotLightIndices += tileIdx;

    xhalf3 result = lightingShader(surfaceData,
                                   aoSample,
                                   depth,
                                   worldPosition,
                                   uniforms,
                                   cameraUniforms,
                                   shadowMap,
                                   dfgLutTex,
                                   envMap,
                                   pointLightBuffer,
                                   spotLightBuffer,
                                   pointLightIndices,
                                   spotLightIndices,
#if USE_SPOT_LIGHT_SHADOWS
                                   spotShadowMaps,
#endif
                                   gEnableDebugView);

#if USE_SCATTERING_VOLUME
    if(!gEnableDebugView)
        result = applyScattering(result, uint2(in.position.xy), in.texCoord, depth, scattering, blueNoiseTexture, uniforms, cameraUniforms);
#endif

    return xhalf4(result, 1);
}
