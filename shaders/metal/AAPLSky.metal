/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Shaders for rendering the skybox
*/

#import "AAPLShaderCommon.h"

//------------------------------------------------------------------------------

constant bool gUseRasterizationRate [[function_constant(RASTERIZATION_RATE_FC_INDEX)]];

//------------------------------------------------------------------------------

// Skybox shader for forward rendering.
fragment xhalf4 skyboxShader(AAPLSimpleTexVertexOut in                      [[ stage_in ]],
                             constant AAPLUniforms & uniforms               [[ buffer(AAPLBufferIndexUniforms) ]],
                             constant AAPLCameraUniforms & cameraUniforms   [[ buffer(AAPLBufferIndexCameraUniforms) ]],
#if SUPPORT_RASTERIZATION_RATE
                             constant rasterization_rate_map_data * rrData  [[ buffer(AAPLBufferIndexRasterizationRateUniforms), function_constant(gUseRasterizationRate) ]],
#endif
                             constant AAPLGlobalTextures & globalTextures   [[ buffer(AAPLBufferIndexFragmentGlobalTextures) ]]
                             )
{
    xhalf3 result = (xhalf3)uniforms.skyColor;

    float2 screenUV = in.texCoord.xy;
#if SUPPORT_RASTERIZATION_RATE
    if (gUseRasterizationRate)
    {
        // We are currently drawing inside compressed space, so we have to fix up screen space.
        rasterization_rate_map_decoder decoder(*rrData);
        screenUV = decoder.map_physical_to_screen_coordinates(screenUV * uniforms.physicalSize) * uniforms.invScreenSize;
    }
#endif

#if USE_SCATTERING_VOLUME
    float linearDepth = linearizeDepth(cameraUniforms, 1.0f);
    xhalf4 scatteringSample;
    {
        constexpr sampler linearSampler(mip_filter::linear, mag_filter::linear, min_filter::linear, address::clamp_to_edge);

        float scatterDepth = zToScatterDepth(linearDepth);
        scatterDepth = saturate(scatterDepth);

        scatteringSample = globalTextures.scattering.sample(linearSampler, float3(screenUV, scatterDepth));

        result = result * scatteringSample.a + scatteringSample.rgb;
    }
#endif

    return xhalf4(result, 0.0f);
}
