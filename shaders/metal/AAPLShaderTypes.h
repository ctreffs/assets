/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Header for generic types shared between Metal shader code and ObjC
*/

#ifdef __METAL_VERSION__
#define NS_ENUM(_type, _name) enum _name : _type _name; enum _name : _type
#define NSInteger metal::int32_t
#else
#import <Foundation/Foundation.h>
#endif

#import <simd/simd.h>

#import "../AAPLConfig.h"

//----------------------------------------------

// Global function constant indices.
#define USE_ALPHA_MASK_FC_INDEX         0
#define TRANSPARENT_FC_INDEX            1
#define TILE_SIZE_FC_INDEX              2
#define DISPATCH_SIZE_FC_INDEX          3
#define ENABLE_DEBUG_VIEW_FC_INDEX      4
#define USE_LIGHT_CLUSTER_FC_INDEX      5
#define RASTERIZATION_RATE_FC_INDEX     6

//----------------------------------------------

// Indices for buffer bindings.
typedef NS_ENUM(NSInteger, AAPLBufferIndex)
{
    AAPLBufferIndexUniforms = 0,
    AAPLBufferIndexCameraUniforms,
#if SUPPORT_RASTERIZATION_RATE
    AAPLBufferIndexRasterizationRateUniforms,
#endif
    AAPLBufferIndexCommonCount,

    AAPLBufferIndexVertexMeshPositions = AAPLBufferIndexCommonCount,
    AAPLBufferIndexVertexMeshGenerics,
    AAPLBufferIndexVertexMeshNormals,
    AAPLBufferIndexVertexMeshTangents,
    AAPLBufferIndexVertexCount,

    AAPLBufferIndexFragmentMaterial = AAPLBufferIndexCommonCount,
    AAPLBufferIndexFragmentGlobalTextures,
    AAPLBufferIndexFragmentLightParams,
    AAPLBufferIndexFragmentChunkViz,
    AAPLBufferIndexFragmentCount,

    AAPLBufferIndexPointLights = AAPLBufferIndexFragmentCount,
    AAPLBufferIndexSpotLights,
    AAPLBufferIndexLightCount,
    AAPLBufferIndexPointLightIndices,
    AAPLBufferIndexSpotLightIndices,

    AAPLBufferIndexComputeEncodeArguments = AAPLBufferIndexCommonCount,
    AAPLBufferIndexComputeCullCameraUniforms,
#if SHADOW_VERTEX_AMP
    AAPLBufferIndexComputeCullCameraUniforms2,
#endif
    AAPLBufferIndexComputeUniforms,
    AAPLBufferIndexComputeMaterial,
    AAPLBufferIndexComputeChunks,
    AAPLBufferIndexComputeChunkViz,
    AAPLBufferIndexComputeExecutionRange,
    AAPLBufferIndexComputeCount,

    AAPLBufferIndexVertexDepthOnlyICBBufferCount            = AAPLBufferIndexVertexMeshPositions+1,
    AAPLBufferIndexVertexDepthOnlyICBAlphaMaskBufferCount   = AAPLBufferIndexVertexMeshGenerics+1,
    AAPLBufferIndexVertexICBBufferCount                     = AAPLBufferIndexVertexCount,

    AAPLBufferIndexFragmentICBBufferCount                   = AAPLBufferIndexFragmentCount,
    AAPLBufferIndexFragmentDepthOnlyICBAlphaMaskBufferCount = AAPLBufferIndexFragmentMaterial+1,
};

// Indices for vertex attributes.
typedef NS_ENUM(NSInteger, AAPLVertexAttribute)
{
    AAPLVertexAttributePosition = 0,
    AAPLVertexAttributeNormal   = 1,
    AAPLVertexAttributeTangent  = 2,
    AAPLVertexAttributeTexcoord = 3,
};

// Indices for members of the AAPLShaderMaterial argument buffer.
typedef NS_ENUM(NSInteger, AAPLMaterialIndex)
{
    AAPLMaterialIndexBaseColor,
    AAPLMaterialIndexMetallicRoughness,
    AAPLMaterialIndexNormal,
    AAPLMaterialIndexEmissive,
    AAPLMaterialIndexAlpha,
    AAPLMaterialIndexHasMetallicRoughness,
    AAPLMaterialIndexHasEmissive,

#if USE_TEXTURE_STREAMING
    AAPLMaterialIndexBaseColorMip,
    AAPLMaterialIndexMetallicRoughnessMip,
    AAPLMaterialIndexNormalMip,
    AAPLMaterialIndexEmissiveMip,
#endif
};

// Indices for members of the AAPLShaderLightParams argument buffer.
typedef NS_ENUM(NSInteger, AAPLLightParamsIndex)
{
    AAPLLightParamsIndexPointLights,
    AAPLLightParamsIndexSpotLights,
    AAPLLightParamsIndexPointLightIndices,
    AAPLLightParamsIndexSpotLightIndices,
    AAPLLightParamsIndexPointLightIndicesTransparent,
    AAPLLightParamsIndexSpotLightIndicesTransparent,
};

// Indices for members of the AAPLGlobalTextures argument buffer.
typedef NS_ENUM(NSInteger, AAPLGlobalTextureIndex)
{
    AAPLGlobalTextureIndexViewDepthPyramid,
    AAPLGlobalTextureIndexShadowMap,
    AAPLGlobalTextureIndexDFG,
    AAPLGlobalTextureIndexEnvMap,
    AAPLGlobalTextureIndexBlueNoise,
    AAPLGlobalTextureIndexPerlinNoise,
    AAPLGlobalTextureIndexSAO,
    AAPLGlobalTextureIndexScattering,
    AAPLGlobalTextureIndexSpotShadows,
};

// Indices for threadgroup storage during tiled light culling.
typedef NS_ENUM(NSInteger, AAPLTileThreadgroupIndex)
{
    AAPLTileThreadgroupIndexDepthBounds,
    AAPLTileThreadgroupIndexLightCounts,
    AAPLTileThreadgroupIndexTransparentPointLights,
    AAPLTileThreadgroupIndexTransparentSpotLights,
    AAPLTileThreadgroupIndexScatteringVolume,
};

// Options for culling visualization.
typedef NS_ENUM(NSInteger, AAPLVisualizationType)
{
    AAPLVisualizationTypeNone,
    AAPLVisualizationTypeChunkIndex,
    AAPLVisualizationTypeCascadeCount,
    AAPLVisualizationTypeFrustum,
    AAPLVisualizationTypeFrustumCull,
    AAPLVisualizationTypeFrustumCullOcclusion,
    AAPLVisualizationTypeFrustumCullOcclusionCull,
    AAPLVisualizationTypeCount
};

// Matrices stored and generated internally within the camera object.
typedef struct AAPLCameraUniforms
{
    // Standard camera matrices.
    simd::float4x4      viewMatrix;
    simd::float4x4      projectionMatrix;
    simd::float4x4      viewProjectionMatrix;

    // Inverse matrices.
    simd::float4x4      invViewMatrix;
    simd::float4x4      invProjectionMatrix;
    simd::float4x4      invViewProjectionMatrix;

    simd::float4        worldFrustumPlanes[6]; // Frustum planes in world space.

    simd::float4        invProjZ;           // A float4 containing the lower right 2x2 z,w block of inv projection matrix (column Major) ; viewZ = (X * projZ + Z) / (Y * projZ + W)
    simd::float4        invProjZNormalized; // Same as invProjZ but the result is a Z from 0...1 instead of N...F; effectively linearizes Z for easy visualization/storage
} AAPLCameraUniforms;

// Uniforms common to most shaders.
typedef struct AAPLUniforms
{
    AAPLCameraUniforms  cullUniforms;       // Uniforms for culling.
    AAPLCameraUniforms  shadowCameraUniforms[SHADOW_CASCADE_COUNT]; // Uniforms for cascade shadows cameras.

    // Previous view projection matrix for temporal reprojection.
    simd::float4x4      prevViewProjectionMatrix;

    // Screen resolution and inverse for texture sampling.
    simd::float2        screenSize;
    simd::float2        invScreenSize;

    // Physical resolution and inverse for adjusting between screen and physical space.
    simd::float2        physicalSize;
    simd::float2        invPhysicalSize;

    // Lighting environment
    simd::float3        sunDirection;
    simd::float3        sunColor;
    simd::float3        skyColor;
    float               exposure;
    float               localLightIntensity;
    float               iblScale;
    float               iblSpecularScale;
    float               emissiveScale;
    float               scatterScale;
    float               wetness;

    simd::float3        globalNoiseOffset;

    simd::uint4         lightIndicesParams;

    // Distance scale for scattering.
    float               oneOverFarDistance;

    // Frame counter and time for varying values over frames and time.
    uint                frameCounter;
    float               frameTime;

    // Debug settings.
    uint                debugView;
    uint                visualizeCullingMode;
    uint                debugToggle;
} AAPLUniforms;

// Point light information.
typedef struct AAPLPointLightData
{
    simd::float4    posSqrRadius;   // Position in XYZ, radius squared in W.
    simd::float3    color;          // RGB color of light.
    uint            flags;          // Optional flags. May include LIGHT_FOR_TRANSPARENT_FLAG.
} AAPLPointLightData;

// Spot light information.
typedef struct AAPLSpotLightData
{
    simd::float4    boundingSphere;     // Bounding sphere for quick visibility test.
    simd::float4    posAndHeight;       // Position in XYZ and height of spot in W.
    simd::float4    colorAndInnerAngle; // RGB color of light.
    simd::float4    dirAndOuterAngle;   // Direction in XYZ, cone angle in W.
    simd::float4x4  viewProjMatrix;     // View projection matrix to light space.
    uint            flags;              // Optional flags. May include LIGHT_FOR_TRANSPARENT_FLAG.

} AAPLSpotLightData;

// Point light information for culling.
typedef struct AAPLPointLightCullingData
{
    simd::float4    posRadius;          // Bounding sphere position in XYZ and radius of sphere in W.
                                        // Sign of radius
                                        //  positive - transparency affecting light
                                        //  negative - light does not affect transparency
} AAPLPointLightCullingData;

// Spot light information for culling.
typedef struct AAPLSpotLightCullingData
{
    simd::float4    posRadius;          // Bounding sphere position in XYZ and radius of sphere in W.
                                        // Sign of radius
                                        //  positive - transparency affecting light
                                        //  negative - light does not affect transparency
    simd::float4    posAndHeight;       // View space position in XYZ and height of spot in W.
    simd::float4    dirAndOuterAngle;   // View space direction in XYZ and cosine of outer angle in W.
} AAPLSpotLightCullingData;
