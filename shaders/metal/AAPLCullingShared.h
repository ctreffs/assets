/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Header for types shared between Metal and ObjC mesh culling code
*/

#ifdef __METAL_VERSION__
#define NS_ENUM(_type, _name) enum _name : _type _name; enum _name : _type
#define NS_OPTIONS(_type, _name) enum _name : _type _name; enum _name : _type
#define NSInteger metal::int32_t
#else
#import <Foundation/Foundation.h>
#endif

// Enum to index the members of the AAPLEncodeArguments argument buffer.
typedef NS_ENUM(NSInteger, AAPLEncodeArgsIndex)
{
    AAPLEncodeArgsIndexCommandBuffer,
    AAPLEncodeArgsIndexCommandBufferDepthOnly,
    AAPLEncodeArgsIndexIndexBuffer,
    AAPLEncodeArgsIndexVertexBuffer,
    AAPLEncodeArgsIndexVertexNormalBuffer,
    AAPLEncodeArgsIndexVertexTangentBuffer,
    AAPLEncodeArgsIndexUVBuffer,
    AAPLEncodeArgsIndexUniformBuffer,
    AAPLEncodeArgsIndexGlobalTexturesBuffer,
    AAPLEncodeArgsIndexLightParamsBuffer,
};

// Results of the culling operation.
typedef NS_ENUM(uint32_t, AAPLCullResult)
{
    AAPLCullResultNotCulled                 = 0,
    AAPLCullResultFrustumCulled             = 1,
    AAPLCullResultOcclusionCulled           = 2,
};

// Function constant indices
#define USE_OCCLUSION_CULLING_FC_INDEX  0
#define ENCODE_ALPHA_MASK_FC_INDEX      1
#define ENCODE_TO_DEPTH_ONLY_FC_INDEX   2
#define ENCODE_TO_MAIN_FC_INDEX         3
#define VISUALIZE_CULLING_FC_INDEX      4
#define PACK_COMMANDS_FC_INDEX          5

#define CULLING_THREADGROUP_SIZE  (128)

// Uniforms for the culling process.
typedef struct AAPLCullUniforms
{
    uint numChunks;         // The number of chunks to process.
    uint offset;            // The offset for writing the chunks.
                            //  Allows thread relative indexing which can be
                            //  reused between opaque and alpha mask.
} AAPLCullUniforms;

// Chunk visualization data.
//  Populated by culling to be applied during rendering.
typedef struct AAPLChunkVizData
{
    uint index;         // Index for chunk - can be used for coloring.
    uint cullType;      // Type of culling for this chunk - AAPLCullResult.
    uint cascadeCount;  // Number of overlapping cascades.
} AAPLChunkVizData;

