//
//  Shaders.metal
//  MetalRendererTestApp Shared
//
//  Created by Christian Treffs on 05.11.18.
//  Copyright © 2018 Christian Treffs. All rights reserved.
//

#include <simd/simd.h>
#include <metal_stdlib>

using namespace metal;

#define kMaxNumLights 4

struct LightUniform {
    float3 position;
    float4 color;
};

struct FrameUniforms {
    float4x4 projectionMatrix;
    float4x4 viewMatrix;
    int lightCount;
};

struct InstanceUniforms {
    float4x4 modelMatrix;
};


struct VertexIn {
    packed_float3 position;
    packed_float3 normal;
    packed_float2 texCoords;
};

struct VertexOut {
    float4 position [[position]];
    float2 texCoords;
    float3 normalDirCamSpace; // normal dir in cam space
    float3 camDirCamSpace; // view (or eye) direction in cam space
    float4 worldPosition;
};


static float3 phongLighting(float3 color,
                            float3 normalDir,
                            float3 camDir,
                            float4 lightColor,
                            float3 lightDir) {
    
    float3 Ia = color;// intensity of the incoming ambient light;
    float3 ρa = float3(0.1); // ambient reflectivity coefficient, controls the amount of ambient light reflected from the surface.
    
    float3 Id = color; // diffuse color intensity
    float3 ρd = 1.0 * lightColor.xyz; // diffuse reflectivity coefficient, controls the relative intensity of diffusely reflected light.
    
    
    float3 Is = float3(1); // intensity of the specular light from the light source
    float3 ρs = 0.8 * lightColor.xyz; // specular reflectivity coefficient, controls the amount of specular reflection
    float f = 16.0; // specular exponent, controls the shininess of the surface by controlling the narrowness of the spread of specularly reflected light.
    
    float Ie = 0.1; // emissivity of a surface, controls how much light the surface emits in the absence of any incident light. // emissive intensity constant Ie. This is equal to the intensity of the light emitted by the surface in addition to the reflected light.
    
    float3 N = normalize(normalDir); // normal unit vector
    float3 L = normalize(lightDir); // light direction unit vector
    float3 V = normalize(camDir); // view/camera/eye direction unit vector
    float3 R = reflect(-L, N); // reflection vector
    
    float3 ambient = ρa * Ia;
    float3 diffuse = ρd * Id * saturate(dot(N, L));
    float3 specular = ρs * Is * pow(saturate(dot(R,V)), f);
    
    
    return float3(ambient + diffuse + specular + Ie);
}


static float3x3 mat3(float4x4 inMat) {
    float3x3 outMat;
    outMat[0].xyz = inMat[0].xyz;
    outMat[1].xyz = inMat[1].xyz;
    outMat[2].xyz = inMat[2].xyz;
    return outMat;
}

vertex VertexOut vertexSimple(constant FrameUniforms &frameUniforms [[ buffer(0) ]],
                              constant InstanceUniforms &instanceUniforms [[ buffer(1) ]],
                              constant LightUniform *lights [[ buffer(2) ]],
                              device VertexIn *vertices [[ buffer(3) ]],
                              uint vertexId [[vertex_id]])
{
    VertexOut out;
    
    // inputs
    float4x4 projectionMat = frameUniforms.projectionMatrix;
    float4x4 viewMat = frameUniforms.viewMatrix;
    
    float4x4 modelMat = instanceUniforms.modelMatrix;
    
    float3 position = vertices[vertexId].position;
    float3 normal = vertices[vertexId].normal;
    float2 uvCoords = vertices[vertexId].texCoords;
    
    // outputs
    out.position = projectionMat * viewMat * modelMat * float4(position, 1.0);
    out.texCoords = uvCoords;
    out.normalDirCamSpace = mat3(viewMat) * mat3(modelMat) * normal;
    float4 worldPosition = modelMat * float4(position, 1.0);
    out.camDirCamSpace = -(viewMat * worldPosition).xyz;
    out.worldPosition = worldPosition;
    
    return out;
}


fragment float4 fragPhong(VertexOut fragmentIn [[stage_in]],
                          constant FrameUniforms &frameUniforms [[ buffer(0) ]],
                          constant InstanceUniforms &instanceUniforms [[ buffer(1) ]],
                          constant LightUniform *lights [[ buffer(2) ]],
                          sampler sampler2d [[sampler(0)]],
                          texture2d<float, access::sample> texBaseColor [[texture(0)]],
                          texture2d<float, access::sample> texDiffuse [[texture(1)]],
                          texture2d<float, access::sample> texSpecular [[texture(2)]],
                          texture2d<float, access::sample> texAmbient [[texture(3)]],
                          texture2d<float, access::sample> texMetallic [[texture(4)]],
                          texture2d<float, access::sample> texRoughness [[texture(5)]],
                          texture2d<float, access::sample> texNormal [[texture(6)]],
                          texture2d<float, access::sample> texReflective [[texture(7)]],
                          texture2d<float, access::sample> texTransparent [[texture(8)]],
                          texture2d<float, access::sample> texAO [[texture(9)]],
                          texture2d<float, access::sample> texIllumniation [[texture(10)]],
                          texture2d<float, access::sample> texEmission [[texture(11)]],
                          texture2d<float, access::sample> texMultiply [[texture(12)]],
                          texture2d<float, access::sample> texDisplacement [[texture(13)]])
{
    
    
    
    // inputs
    //float4 position = fragmentIn.position;
    float2 uvCoords = fragmentIn.texCoords;
    float3 normalDirCamSpace = fragmentIn.normalDirCamSpace; // normal dir in cam space
    float3 camDirCamSpace = fragmentIn.camDirCamSpace;
    float4 worldPosition = fragmentIn.worldPosition;
    int lightCount = frameUniforms.lightCount;
    float4x4 viewMat = frameUniforms.viewMatrix;
    int numLights = lightCount < kMaxNumLights ? lightCount : kMaxNumLights;
    
    
    float4 colorTex = texBaseColor.sample(sampler2d, uvCoords).rgba;
    //float4 uvSpecular = texSpecular.sample(sampler2d, uvCoords).rgba;
    //float4 uvAmbient = texAmbient.sample(sampler2d, uvCoords).rgba;
    
    float3 color = colorTex.rgb;
    
    float3 outColor = float3(0);
    for ( int i = 0; i < numLights; i++ ) {
        LightUniform light = lights[ i ];
        
        // light direction in cam space
        float3 lightDirCamSpace = mat3(viewMat) * (light.position - float3(worldPosition));
        outColor += phongLighting(color,
                                  normalDirCamSpace,
                                  camDirCamSpace,
                                  light.color,
                                  lightDirCamSpace);
    }
    
    
    return float4(outColor, 1.0);
}



